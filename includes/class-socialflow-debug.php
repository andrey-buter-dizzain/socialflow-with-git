<?php 

if ( !function_exists( 'sf_debug' ) ) {
	function sf_debug( $msg, $data = array() ) {
		SF_Debug::get_instance()->log( $msg, $data, 'debug' );
	}
}

if ( !function_exists( 'sf_log' ) ) {
	function sf_log( $msg, $data = array() ) {
		SF_Debug::get_instance()->log( $msg, $data, 'post' );
	}
}

if ( !function_exists( 'sf_log_post' ) ) {
	function sf_log_post( $msg, $post ) {
		SF_Debug::get_instance()->log_post( $msg, $post );
	}
}

/**
 * 
 */
class SF_Debug
{
	protected static $instance;

	protected $is_logged = false;

	protected $debug = true;

	protected $folder = 'socialflow';

	protected $notice = array();

	protected $files = array(
		'post',
		'debug'
	);

	protected $logs = array();

	protected function __construct()
	{
		add_action( 'shutdown', array( $this, 'on_wp_die' ) );

		add_action( 'init', array( $this, 'on_init' ) );

		add_action( 'admin_notices', array( $this, 'admin_notices' ) );
	}

	function on_init()
	{
		global $socialflow;
		
		// $this->debug = wp_validate_boolean( $socialflow->options->get( 'debug_mode', false ) );

		if ( !$this->debug )
			return;

		if ( !defined( 'SF_DEBUG' ) )
			define( 'SF_DEBUG', true );

		$this->test_log_dir();
	}

	function test_log_dir()
	{
		if ( !wp_mkdir_p( $this->get_path() ) )
			return $this->add_notice( "Log folder doesn't created" );

		if ( !wp_is_writable( $this->get_path() ) )
			return $this->add_notice( 'Log folder is not writable' );

		foreach ( $this->files as $file ) {
			if ( file_exists( $this->get_log_path( $file ) ) )
				continue;

			if ( !$this->write_file( $file, '' ) )
				return $this->add_notice( "$file file does not created" );
		}

	}

	protected function add_notice( $msg )
	{
		$this->notice = $msg;
	}

	function on_wp_die()
	{
		if ( !$this->is_logged )
			return;

		foreach ( $this->files as $file ) {
			if ( !isset( $this->logs[ $file ] ) )
				continue;

			if ( empty( $this->logs[ $file ] ) )
				continue;

			$this->write_file( $file, "" );
			$this->write_file( $file, "--------------" );
			$this->write_file( $file, "" );
		}
	}

	public function get_instance()
	{
		if ( !self::$instance )
			self::$instance = new self();

		return self::$instance;
	}

	public function log( $msg, $data = array(), $file = 'post' )
	{		
		// $debug = wp_validate_boolean( $socialflow->options->get( 'debug_mode', false ) );

		if ( !$this->debug )
			return;

		$this->is_logged = true;

		$date = date( "Y-m-d H:i:s" );

		if ( $msg ) {
			if ( $data )
				$msg .= "\n". print_r( $data, true );

			$msg = "$date: $msg";
		}

		$this->write_file( $file, $msg );
	}

	public function log_post( $msg, $post )
	{
		if ( is_object( $post ) ) {
			if ( 'post' != $post->post_type )
				return;

			if ( in_array( $post->post_status, array( 'new', 'any', 'auto-draft' ) ) )
				return;

			$post_id = $post->ID;
		} else {
			$post_id = $post;
		}

		$this->log( "post_ID: {$post_id} - $msg" );
	}

	protected function get_path()
	{
		$dir = wp_upload_dir();

		return "{$dir['basedir']}/{$this->folder}";
	}

	protected function get_log_path( $file )
	{
		return $this->get_path() ."/{$file}.log";
	}

	protected function write_file( $file, $msg = '' )
	{
		if ( $msgs )
			$this->logs[ $file ][] = $msgs;

		$fp = @fopen( $this->get_log_path( $file ), 'a' );

		if ( !$fp )
			return false;

		mbstring_binary_safe_encoding();

		$msg = "$msg\n";

		$data_length = strlen( $msg );

		$bytes_written = fwrite( $fp, $msg );

		reset_mbstring_encoding();

		fclose( $fp );

		if ( $data_length !== $bytes_written )
			return false;

		return true;
	}

	function admin_notices()
	{
		if ( !$this->notice )
			return;
	 ?>
		<div class="notice notice-warning is-dismissible">
			<p><b><?php _e( 'Socialflow Debug:' ) ?></b> <?php echo $this->notice ?> - <i><?php _e( 'File system permissions error.' ) ?></i></p>
			<p>Log file path:   <?php echo str_replace( WP_CONTENT_DIR, '', $this->get_log_path( 'post'  ) ) ?></p>
			<p>Debug file path: <?php echo str_replace( WP_CONTENT_DIR, '', $this->get_log_path( 'debug' ) ) ?></p>
		</div>
	<?php
	}
}
if ( is_admin() )
	SF_Debug::get_instance();